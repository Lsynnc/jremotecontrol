package com.molchat.jremotecontrol.core;

import com.molchat.jremotecontrol.settings.Settings;
import com.molchat.jremotecontrol.web.WebMan;
import com.molchat.jremotecontrol.web.impl.WebManImpl;

import java.io.IOException;

/**
 * Main program manager.
 *
 * @author Valentyn Markovych, Gorilla
 */
public class MainMan {

    private WebMan webMan;
    private Settings settings;

    public MainMan() throws Exception {

        init();
        start();
        try {
            Thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void start() throws IOException {

        webMan.start();
    }


    private void init() {

        settings = Settings.getInstance();

        webMan = new WebManImpl();
        webMan.configure(settings);
    }
}
