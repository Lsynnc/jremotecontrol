package com.molchat.jremotecontrol.core;


import com.molchat.jremotecontrol.settings.Settings;
import com.molchat.jremotecontrol.settings.SettingsConstants;
import com.molchat.jremotecontrol.web.WebMan;
import com.molchat.jremotecontrol.web.impl.WebManImpl;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

/**
 * Main program launcher.
 *
 * @author Valentyn Markovych, Gorila
 */
public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        // Configuring logger
        PropertyConfigurator.configure("res/log4j.properties");

        new MainMan();
    }
}
