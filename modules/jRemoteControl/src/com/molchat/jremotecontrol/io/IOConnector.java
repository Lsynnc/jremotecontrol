package com.molchat.jremotecontrol.io;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class IOConnector {

    public IOProcessor getIoProcessor() {
        return ioProcessor;
    }

    public void setIoProcessor(IOProcessor ioProcessor) {
        this.ioProcessor = ioProcessor;
    }

    private IOProcessor ioProcessor;


    public void listen() {

    }

    public void exit() {

    }
}
