package com.molchat.jremotecontrol.io.impl;

import com.molchat.jremotecontrol.io.IOOperation;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class IOOperationImpl implements IOOperation {

    private Object inObj;

    private Object outObj;

    public Object getInObj() {
        return inObj;
    }

    public void setInObj(Object inObj) {
        this.inObj = inObj;
    }

    public Object getOutObj() {
        return outObj;
    }

    public void setOutObj(Object outObj) {
        this.outObj = outObj;
    }
}
