package com.molchat.jremotecontrol.io;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class IOMan {

    private static final IOMan INSTANCE = new IOMan();


    private IOMan() {

    }


    public void startListen() {

    }

    public void stopListen() {

    }

    public void exit() {

    }
}
