package com.molchat.jremotecontrol.io;

import com.molchat.jremotecontrol.io.impl.IOOperationImpl;

/**
 * @author Valentyn Markovych, Gorilla
 */
public interface IOProcessor {

    public void process(IOOperationImpl operation);
}
