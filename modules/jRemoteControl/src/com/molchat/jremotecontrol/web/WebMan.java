package com.molchat.jremotecontrol.web;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Web manager. Renders web pages and answers AJAX requests.
 *
 * @author Valentyn Markovych, Gorilla
 */
public interface WebMan {

    public void start() throws IOException;

    public void exit();

    public void configure(Properties properties);
}
