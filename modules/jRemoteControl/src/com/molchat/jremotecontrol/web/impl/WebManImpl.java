package com.molchat.jremotecontrol.web.impl;

import com.molchat.jremotecontrol.settings.SettingsConstants;
import com.molchat.jremotecontrol.web.WebMan;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class WebManImpl implements WebMan {

    private HttpServer server;
    private ExecutorService executorService;

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    private Properties properties;


    @Override
    public void start() throws IOException {

        String portStr = properties.getProperty(SettingsConstants.LISTEN_TO_PORT_KEY, "8080");
        int port = Integer.parseInt(portStr);

        InetSocketAddress addr = new InetSocketAddress(port);
        server = HttpServer.create(addr, 0);

        server.createContext("/", new WebHandler());
        executorService = Executors.newCachedThreadPool();
        server.setExecutor(executorService);
        server.start();
        System.out.println("Server is listening on port " + port);
    }

    @Override
    public void exit() {

    }

    @Override
    public void configure(Properties properties) {
        setProperties(properties);
    }
}
