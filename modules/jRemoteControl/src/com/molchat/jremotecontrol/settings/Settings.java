package com.molchat.jremotecontrol.settings;

import com.molchat.jremotecontrol.core.Main;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class Settings extends Properties {

    private static final Logger LOG = Logger.getLogger(Settings.class);

    private static final String DEFAULT_CONFIG_FILE = "res/application.properties";

    private static final Settings instance = new Settings();

    private void init() {

        InputStream inputStream = Main.class.getClassLoader().getResourceAsStream(DEFAULT_CONFIG_FILE);
        loadDefaults();
        if (inputStream != null) {
            try {
                LOG.debug("Reading properties from " + DEFAULT_CONFIG_FILE);
                this.load(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadDefaults() {

//        this.setProperty(SettingsConstants.LISTEN_TO_IP_KEY, "0.0.0.0");
        this.setProperty(SettingsConstants.LISTEN_TO_PORT_KEY, "8080");
    }

    private Settings() {
        // Closed constructor
        super();
        init();
    }


    public static Settings getInstance() {
        return instance;
    }
}
