package com.molchat.jremotecontrol.settings;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class SettingsConstants {

//    public static final String LISTEN_TO_IP_KEY = "listen.ip";
    public static final String LISTEN_TO_PORT_KEY = "listen.port";
}
